#!/bin/bash

# available imagetypes "primedisk-image installer-image"
imagetype="primedisk-image"

# possible suites "ascii beowolf chimaera daedalus excalibur"
# available suites "chimaera daedalus excalibur"
suites="daedalus"
#suites="daedalus excalibur"

# possible environments "phosh sway plasma-mobile maemo-hildon"
# available environments "phosh sway plasma-mobile"
environments="phosh"

# available devices "pinephone rpi4 amd64 amd64-free pinephonepro pinetab pinetab2 librem5 sdm845 sm7225"
# frequent devices "pinephone rpi4 amd64-free"
devices="pinephone rpi4 amd64-free"
# other possible build target devices (most likely invalid) "sunxi rockchip qcom qcon-wip"

# fakemachine does not like loopback 127.0.0.1
# if you have multiple ip addresses this script selects the first one for use
# apt-cacher-ng is normally listening on port 3142 on every interface. it will fail with timeout if not running.
ip=` ip a s|grep inet|grep -v inet6|grep -v "inet 127"|head -1| sed "~s/\/.*//"| sed "~s/.* //"`

echo "The ip we will use for caching is "$ip" on port 3142"
echo "--------------------------------------------------------------"
echo

cd ..

for i in $imagetype; do

    for s in $suites; do

        for e in $environments; do

            for d in $devices; do

                echo "==============================================================="
                echo "Checking "$d" for "$e" "$i" "$s
                echo "---------------------------------------------------------------"

		if [ "$i" = "installer-image" ]; then
		    iparam="-o"
		    ifile="-installer"
		elif [ "$i" = "primedisk-image" ]; then
		    iparam=""
		    ifile=""
		else
		    echo "we should never reach here unless imagetype is misspelled: "$i
		fi

                if [ -f out/movuan$ifile-$d-$e-$s-????????.???.xz -a "$1" != "--force-overwrite" ]; then

                    echo "$i found movuan$ifile-$d-$e-$s, will not rebuild"
		    ls -l out/movuan$ifile-$d-$e-$s-????????.*.xz
		    echo
        	    echo "if you would like to force rebuilding all images"
        	    echo "stop the current building process then run again, "
        	    echo "using the \"--force-overwrite\" parameter"
		    echo

                else

                    echo -n $i" - "
                    ./build.sh -d -s -i $iparam -z -t $d \
                	    -e $e \
                	    -M "http://deb.devuan.org/merged" \
                	    -f http://$ip:3142 -h http://$ip:3142 \
                	    -x $s -S $s \
                	    -u movuan -p 1234 -H movuan

                    echo "removing uncompressed images"
                    rm -f out/movuan-$d-$e-$s-????????.img
                    #rm -f out/movuan-$d-$e-$s-????????.img.bmap
                    rm -f out/movuan-installer-$d-$e-$s-????????.img
                    #rm -f out/movuan-installer-$d-$e-$s-????????.img.bmap
                    rm -f out/movuan-$d-$e-$s-????????.boot-*.img
                    rm -f out/movuan-$d-$e-$s-????????.rootfs.img

                fi

            done

        done

    done

done
