![Logo](screenshots/movuan-logo.png)
#

A set of [debos](https://github.com/go-debos/debos) recipes for building a
devuan-based image for mobile phones, initially targetting Pine64's PinePhone.

The default user is `movuan` with password `1234`.

This project attempts addressing two separate issues
```
    1. Building on a Devuan system without systemd
    2. Creating a mobile os that does not depend on systemd
```

Why?
Because I would like to avoid depending on controversial 
do-it-all, single-point-of-entry style work by Lennart Poettering.

Is it still depending on other unavoidable less-then-stellar choices?
Yes, but for now it is a starting point.

Based on the excellent work by MobianTeam as shown in:
https://salsa.debian.org/Mobian-team/mobian-recipes

Builds on top of the enormous work for a distribution that 
does not need systemd from the Devuan team https://www.devuan.org.

Thank you Debian, Linux and GNU for over 30 years of freedom respecting computing!

![Screenshot](screenshots/Screenshot1.png)
![Screenshot](screenshots/Screenshot2.png)
![Screenshot](screenshots/Screenshot3.png)

Open [Screenshots Gallery](./screenshots/) for more images.

This is at this moment highly experimental.
Please check [Status](Status) for issues before flashing anything.

If you get a brick, you are on your own.

## Reason for forking Mobian instead of directly contributing upstream

Basing on the Devuan implies changes that are not suitable to ever get into the 
mobian-recipes repo. Their meta packages are all deeply dependent on systemd.


## Prepare your build system

To build the image, you need to have `debos` and `bmaptool`. 

On a Devuan-based system, there is no debos due to dependency on systemd-containers

One can use docker instead - downloads ~500 MB image before downloading any build files

To use docker, run it under sudo or add the user the root group

```
sudo apt-get install docker.io
# you can perform a quick test for your docker environment as below
# sudo docker run hello-world
```

Use a caching proxy for your deb files to avoid excessive downloads for repeated rebuilds
This can run on the localhost

```
apt-get install apt-cacher-ng
```

If using options -f and -h, the deb files will be cached under /var/cache/apt-cacher-ng

If you want to build an image for a Qualcomm-based device, additional packages
are required inside the docker image, which you can install with the following commands:

Similarly, if you want to use F2FS for the root filesystem (which isn't such a
good idea, as it has been known to cause corruption in the past), you'll need to
install `f2fs-tools` in the docker image as well.

There must be better ways of adding packets to a docker image, yet this worked for me:

```
echo "apt-get update; apt-get -y install android-sdk-libsparse-utils f2fs-tools yq"\
 |sudo docker run --name temp -i --entrypoint /bin/sh godebos/debos -s
sudo docker commit temp godebos/debos
sudo docker rm temp
sudo docker run --name temp -i --entrypoint /usr/local/bin/debos godebos/debos
sudo docker commit temp godebos/debos
sudo docker rm temp
```

Do note these instructions are based on Devuan 5 (Daedalus) 

The build system will cache and re-use it's output files. Docker containers will not 
however cache downloaded deb files. 

To create a fresh build remove `*.tar.gz`, `*.sqfs` and `*.img` before starting the build.

The process is done in two stages
    1. create an architecture-specific rootfs
    2. create the device-specific image based on the previous rootfs
There can be multiple devices sharing the same architecture (ex. arm64)

If you already have a good rootfs and want to re-generate the image without re-generating
the rootfs, add option -i to the build.sh statement.

## Build

In order to build an image, browse to  the `movuan-recipes` folder and 
use the following sytax replacing the proxyhost with your ip address

```
sudo ./build.sh -d -D -s -t pinephone \
		-M "http://deb.devuan.org/merged"  \
		-f http://proxyhost:3142 -h http://proxyhost:3142 \
		-x daedalus -S daedalus \
		-u movuan -p 1234 -H movuan 
```

Note: you cannot use localhost or 127.0.0.1 - issues with fakemachine

### Building QEMU image

You can build a QEMU x86_64 image by using the `-t amd64` flag

```
sudo ./build.sh -d -D -s -t amd64 \
		-M "http://deb.devuan.org/merged"  \
		-f http://proxyhost:3142 -h http://proxyhost:3142 \
		-x daedalus -S daedalus \
		-u movuan -p 1234 -H movuan
```

The resulting files are raw images. You can start qemu like so:

```
qemu-system-x86_64 -drive format=raw,file=<imagefile.img> -enable-kvm \
    -cpu host -vga virtio -m 2048 -smp cores=4 \
    -drive if=pflash,format=raw,readonly=on,file=/usr/share/OVMF/OVMF_CODE.fd
```

UEFI firmware files are available in Debian thanks to the
[OVMF](https://packages.debian.org/sid/all/ovmf/filelist) package.
Comprehensive explanation about firmware files can be found at
[OVMF project's repository](https://github.com/tianocore/edk2/tree/master/OvmfPkg).

You may also want to convert the raw image to qcow2 format
and resize it like this:

```
qemu-img convert -f raw -O qcow2 <raw_image.img> <qcow_image.qcow2>
qemu-img resize -f qcow2 <qcow_image.qcow2> +20G
```

### Further Process
After creating the typical image with this project, the next step is customizing it with 
https://gitlab.com/l2385/movuan/customizing-movuan-under-host-mounting which, installs a chromium 
browser along with additional software (proxies and caching dns) and a number of privacy tweaks.

### Other possible build targets

Aside from pinephone and amd64 described above, the -t parameter can also accept:
pinephonepro, pinetab, sunxi, pinetab2, librem5, sdm845, sm7225, amd64-free, rpi4

### Environments

The -e parameter can be used with either plasma-mobile or phosh to build the corresponding type of image

### Other possible Environments

There are also other values the -e parameter can be used with, like sway that builds an experimental only image.
This currently logs in the root user without any password checking, so it really can only be used for debugging.

## Install

Insert a MicroSD card into your computer, and type the following command:

```
sudo bmaptool copy <image> /dev/<sdcard>
```

or:

```
sudo dd if=<image> of=/dev/<sdcard> bs=1M status=progress
```

*Note: Make sure to use your actual SD card device, such as `mmcblk0` instead of
`<sdcard>`.*

**CAUTION: This will format the SD card and erase all its contents!!!**


## Upgrade process

Since there is no custom repository you need to flash every new build and start
over. Successful changes should make their way back asap.

# License

This software is licensed under the terms of the GNU General Public License,
version 3.

Anybody is welcome to fork this project, or use it in any GPL compliant way. Use a different name though, change the logo, 
and notify me back to review if it can be incorporated back. If you want to just mirror the code and provide additional 
pre-built images on a different server, there is no need to change the logo/name. Just make sure to prominently point to 
the original location.

This project has never received any monetary contributions of any kind, from any party. It was all developed in my own 
free time and fully with my own resources. I consider it as my contribution/donation to the free software movement. 
Hope to be useful. Performed a reasonable effort to clarify licensing for every sub-project and to link only GPL/OpenSource 
licensed software, yet if you plan to distribute you should research licensing individually for every sub-project.
