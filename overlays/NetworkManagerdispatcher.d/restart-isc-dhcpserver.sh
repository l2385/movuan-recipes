#!/bin/sh

# This script restarts the dhcp server whenever usb0 is connected
# on a NetworkManager based system. It should be located under:
# /etc/NetworkManager/dispatcher.d/restart-isc-dhcpserver.sh

# SPDX-FileCopyrightText: 2025 lxb1@yahoo.com <lxb1@yahoo.com>
#
# SPDX-License-Identifier: GPL-3.0
#
# Copyright (C) 2025 lxb1@yahoo.com
# License GPL3
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

case "$2" in
    up|dhcp4-change)
	logger -s "NM Script $0 triggered for connection of $1 with state $2"
	if [ "$1" = "usb0" ]; then
	    /etc/init.d/isc-dhcp-server restart
	    exit 0
	fi
	;;
    *)
	exit 0
	;;
esac
