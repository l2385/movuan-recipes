#!/bin/sh
# Homepage: https://gitlab.com/mobian1/mobile-usb-networking

# Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
# Upstream-Name: mobile-usb-networking
# Upstream-Contact: Arnaud Ferraris <arnaud.ferraris@gmail.com>
#Source: https://gitlab.com/mobian1/mobile-usb-networking

# Copyright: 2022 Arnaud Ferraris <arnaud.ferraris@gmail.com>
# License: GPL-3.0-or-later

# License: GPL-3.0-or-later
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  .
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  .
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#  .
#  On Debian systems, the full text of the GNU General Public License
#  version 3 can be found in the file `/usr/share/common-licenses/GPL-3'.

set -e

if [ ! -e /etc/NetworkManager/system-connections/USB.nmconnection ]; then
    # Create network connection
    nmcli connection add con-name USB \
                         ifname usb0 \
                         type ethernet \
                         ip4 192.168.0.1/24

    # Set priorities so it doesn't take precedence over WiFi/mobile connections
    nmcli connection modify USB ipv4.route-metric 1500
    nmcli connection modify USB ipv4.dns-priority 150

    # Share connection so it can be used for tethering
    nmcli connection modify USB ipv4.method shared
fi
