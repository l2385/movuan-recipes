#!/bin/sh
# Quectel EG25 modem
# /etc/init.d/Quectel EG25 modem
# Quectel EG25 modem
#
### BEGIN INIT INFO
# Provides:          Quectel EG25 modem
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       Quectel EG25 modem
### END INIT INFO

test -f /lib/lsb/init-functions || exit 1
. /lib/lsb/init-functions

PIDFILE="/var/run/QuectelEG25modem.pid"
LOGFILE="/var/log/QuectelEG25modem.log"
SCRIPT="/usr/bin/eg25-manager"
RUNAS=""
NAME="QuectelEG25modem"

start() {
  if [ -f "$PIDFILE" ] && [ -s "$PIDFILE" ] && kill -0 $(cat "$PIDFILE"); then
    log_action_msg "Service $NAME already running, PID:" $(cat "$PIDFILE") >&2
    return 1
  fi
  echo "Starting service $NAME…" >&2
  local CMD="$SCRIPT &> \"$LOGFILE\" & echo \$!"
  su -c "$CMD" $RUNAS > "$PIDFILE"

  sleep 2
  PID=$(cat "$PIDFILE")

    local PGREPRES
    if [ "$RUNAS" = "" ]
    then
	PGREPRES=$(pgrep -f "$SCRIPT" > /dev/null)
    else
	PGREPRES=$(pgrep -u $RUNAS -f "$SCRIPT" > /dev/null)
    fi

    if $PGREPRES
    then
      log_action_msg "$NAME is now running, the PID is $PID"
      log_end_msg $?
    else
      log_action_msg "Error! Could not start $NAME, check $LOGFILE for more information."
      log_end_msg $?
    fi
}

stop() {
  if [ ! -f "$PIDFILE" ] || ! kill -0 $(cat "$PIDFILE"); then
    log_action_msg "Service $NAME is not running" >&2
    return 1
  fi
  log_action_begin_msg "Stopping $NAME service…" >&2
  kill -15 $(cat "$PIDFILE") && rm -f "$PIDFILE"
  log_action_msg "Service stopped" >&2
  log_end_msg $?
}

status() {
    printf "%-50s" "Checking $NAME..."
    if [ -f "$PIDFILE" ] && [ -s "$PIDFILE" ]; then
        PID=$(cat "$PIDFILE")
            if [ -z "$(ps axf | grep ${PID} | grep -v grep)" ]; then
                printf "%s\n" "The process appears to be dead but pidfile still exists"
            else    
                echo "Running, the PID is $PID"
            fi
    else
        printf "%s\n" "Service not running"
    fi
}


case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;

	status)
		status
		;;
	restart)
		stop
		start
		;;
	*)
		echo "Usage: $0 {start|stop|reload|status|restart}"
		exit 1
		;;
esac

exit 0
