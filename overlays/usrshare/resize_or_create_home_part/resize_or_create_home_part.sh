#!/bin/bash

# Resize or create home partition using maximum available disk space on primary disk
# ----------------------------------------------------------------------------------
# determine primary disk based on where the root partition is mounted from (mmcblk0 or sda or vda)
# if /home already present, attempt to resize it, otherwise create it on the same drive as the root

# SPDX-FileCopyrightText: 2025 lxb1@yahoo.com <lxb1@yahoo.com>
#
# SPDX-License-Identifier: GPL-3.0
#
# Copyright (C) 2025 lxb1@yahoo.com
# License GPL3
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

echo "Running this script accidentally on a system that has been in use could lead to data loss"
# need to make every effort to verify this runs during the first boot only and never again
echo "!!! do not run this manually unless you ***really*** know what you are doing !!!"
echo "As a safety, this script will attempt to auto-disable itself after first execution"
# auto-disable by uncommenting next line
# exit # auto-disabled

rootpart=`mount|grep "on / type"| cut -f 1 -d " "| cut -f 3 -d "/"`
homepart=`mount|grep "on /home type"| cut -f 1 -d " "| cut -f 3 -d "/"`

minextradiskspace="2000000"
# minimum disk space before creating a new disk partition
# disk space on my drive after creation, without any initial resizing was: 1044992

if [ "$homepart" = "" ]; then
    homepartdisk=`echo $rootpart | sed "~s/[0-9]*$//gi"`
    homepartpnum=`echo $rootpart | sed "~s/.*[a-z]\([0-9]*\)$/\1/i"`
    homepartpnum=$((homepartpnum+1))
    homepart=$homepartdisk$homepartpnum
    homepartdisk=`echo $homepartdisk | sed "~s/p$//gi"`
    missinghomepart="true"
    echo "did not find home partition on any disk, creating..." | tee /var/log/part-resize.log
else
    missinghomepart="false"
    homepartdisk=`echo $homepart | sed "~s/[0-9]*$//gi"`
    homepartpnum=`echo $homepart | sed "~s/.*[a-z]\([0-9]*\)$/\1/i"`
    rootpartdisk=`echo $rootpart | sed "~s/[0-9]*$//gi"`
    if [ "$homepartdisk" = "$rootpartdisk" ]; then
	echo "found home partition on the same disk as root partition, resizing..." | tee /var/log/part-resize.log
    else
	echo "found home partition on a different disk than root partition, resizing..." | tee /var/log/part-resize.log
    fi
    homepartdisk=`echo $homepartdisk | sed "~s/p$//gi"`
fi

# echo "homepartdisk is:$homepartdisk"
# echo "homepartpnum is:$homepartpnum"
echo "homepart will be: $homepart" | tee /var/log/part-resize.log
echo "missinghomepart is: $missinghomepart"| tee /var/log/part-resize.log

cd /

if [ "$missinghomepart" = "false" ]; then
    echo "unmounting home partition ..." | tee /var/log/part-resize.log
    fuser -km /home 2>&1 | tee /var/log/part-resize.log
    umount /home 2>&1 | tee /var/log/part-resize.log

    echo "before resize partition table looks like this:" | tee /var/log/part-resize.log
    parted -s /dev/$homepartdisk print 2>&1 | tee /var/log/part-resize.log

    parted -s -f -m /dev/$homepartdisk resizepart $homepartpnum 100%
    # bash -c "echo 'resizepart'; echo F ; echo '$homepartpnum 100%'; echo Y"|parted ---pretend-input-tty -a opt /dev/$homepartdisk 2>&1 | tee /var/log/part-resize.log
    ## Error: The resize command has been removed in parted 3.0 

    echo "after resize partition table looks now like this:" | tee /var/log/part-resize.log
    parted -s /dev/$homepartdisk print 2>&1 | tee /var/log/part-resize.log

    partprobe
    # alternative echo 1 > /sys/class/block/${disk//*\/}/device/rescan

    fsck -yf /dev/$homepart
    resize2fs /dev/$homepart

    mount /home

else
    # create home partition
    echo "creating home partition, then adding the new entry into fstab" | tee /var/log/part-resize.log

    partlist=`parted -s -m /dev/$homepartdisk unit b print`
    disksize=`echo $partlist | sed "s/;/\n;/g" | sed "s/B:/:/g" | grep $homepartdisk | cut -f 2 -d ":"`
    useddisksize="0"
    sizelist=`echo $partlist | sed "s/;/\n;/g" | sed "s/B:/:/g" | grep -v $homepartdisk | grep -v BYT | cut -f 4 -d ":" | grep -v ";" `
    for i in $sizelist; do 
#	echo "current size is: $i"
	useddisksize=$(($i + $useddisksize))
    done
    startnewpart=`echo $partlist | sed "s/;/\n;/g" | sed "s/B:/:/g" | grep -v $homepartdisk | grep -v BYT | cut -f 3 -d ":" | grep -v ";" | tail -1 `
    startnewpart=$((1 + $startnewpart))
    homefoldersize=`du /home -s --block-size=1 | cut -f 1`

    echo "before creating /home, partition table looks now like this:" | tee /var/log/part-resize.log
    echo $partlist | tee /var/log/part-resize.log 
    echo "disksize=$disksize" | tee /var/log/part-resize.log
    echo "useddisksize=$useddisksize" | tee /var/log/part-resize.log
    echo "homefoldersize=$homefoldersize" | tee /var/log/part-resize.log
    echo "startnewpart=$startnewpart" | tee /var/log/part-resize.log
    echo "sparediskspace=$(($disksize - $useddisksize - $homefoldersize))" | tee /var/log/part-resize.log

    # exit with error "not enough disk space" if $disksize - $useddisksize - $homefoldersize less than 0
    if [ "$(($disksize - $useddisksize - $homefoldersize))" -le "$minextradiskspace" ]; then
	echo "Error: Not enough disk space"
	exit
    fi

    # create partition using existing disk space
    echo "executing parted -s -f -m /dev/$homepartdisk unit b mkpart primary $startnewpart 100%" | tee /var/log/part-resize.log
    partedresult=`parted -s -f -m /dev/$homepartdisk unit b mkpart primary $startnewpart 100% 2>&1` 
    if [ ! "$?" = "0" ]; then
	# exit if error (display the actual error)
	echo "partedresult=$partedresult" | tee /var/log/part-resize.log
	exit
    fi
    echo $partedresult >> /var/log/part-resize.log

    # format partition; ; exit if error (display the actual error)
    echo "executing mkfs.ext4 /dev/$homepart" | tee /var/log/part-resize.log
    mkfs.ext4 /dev/$homepart 2>&1 | tee /var/log/part-resize.log

    # insert home entry into fstab if not already there, or if only subdirectories may be already mounted below /home/
    ## subdirectory without /home check still needs to be implemented
    if [ "`grep \"^/home\" /etc/fstab`" = "" ] ; then
	echo "/dev/$homepart					/home		ext4	defaults	0	2" >> /etc/fstab
	echo "/dev/$homepart					/home		ext4	defaults	0	2" | tee /var/log/part-resize.log
    fi

    # mount partition to temporary location
    mount /dev/$homepart /mnt 2>&1 | tee /var/log/part-resize.log
    # move or copy files from /home
    cp -aR /home/* /mnt 2>&1 | tee /var/log/part-resize.log
    if [ ! "$?" = "0" ]; then
	echo "only in case of full success, it can also remove original files" | tee /var/log/part-resize.log
	# only in case of full success, it can also remove original files
	# rm -rf /home/* 2>&1 | tee /var/log/part-resize.log
    fi
    # unmount partition
    umount /mnt 2>&1 | tee /var/log/part-resize.log

    # mount partition to the normal location
    mount /home 2>&1 | tee /var/log/part-resize.log

fi

echo "Disabling execution of $0" | tee /var/log/part-resize.log

sed -i --follow-symlinks "~s/^# exit # auto-disabled/exit # auto-disabled/" $0 2>&1 | tee /var/log/part-resize.log

if [ -L /etc/boot.d/`basename $0` ] ; then
    echo "Removing symlink to "`basename $0`" from /etc/boot.d/ as well" | tee /var/log/part-resize.log
    rm -f /etc/boot.d/$0 2>&1 | tee /var/log/part-resize.log
fi

echo "Removing any other symlinks from /etc/boot.d/ that point to this same script" 2>&1 | tee /var/log/part-resize.log
find -L /etc/boot.d/ -xtype l -samefile $0 -exec rm {} \;  2>&1 | tee /var/log/part-resize.log
