# Resize or Create home partition

Script that starts using the extra disk space by either resizing or creating a new /home partition

When creating a new image for Movuan (or other distributions), it has a pre-defined size.
This image gets copyed on larger drives (like SD for example) with dd, leaving unused space.
On Debian and most other systemd based distributions, the sd image can be resized during first boot,
in order to get maximum usable space. As systemd is not available on Devuan, other methods have to
be employed for the same purpose. Hence the need for this script.

## Original location URL
https://gitlab.com/l2385/movuan/resize_or_create_home_part

## License
SPDX-FileCopyrightText: 2022-2025 lxb1@yahoo.com <lxb1@yahoo.com>
SPDX-License-Identifier: GPL-3.0

Copyright (C) 2022-2025 lxb1@yahoo.com
License GPL3

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.
